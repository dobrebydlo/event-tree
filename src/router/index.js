import Vue from 'vue'
import VueRouter from 'vue-router'
import Tree from '../views/Tree.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Tree',
    component: Tree
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
