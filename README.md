# Test task
Render timeline

# Solution
* There is a view containing the data, and an EventTree component which in turn refers the Event component.
* The only route in the app (root) is mapped to that view.

## View
* src/views/Tree.vue

## Components
* src/components/Event.vue — Single event component
* src/components/EventTree.vue — Event tree component

# Vue
Vue server runs as usual, no changes were made.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
